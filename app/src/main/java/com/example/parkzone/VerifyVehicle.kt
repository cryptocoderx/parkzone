package com.example.parkzone

import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.example.parkzone.FirebaseInstance.Companion.firestoreDB
import com.example.parkzone.databinding.FragmentVerifyVehicleBinding
import com.google.firebase.Timestamp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event.SIGN_UP
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class VerifyVehicle : Fragment() {

    private lateinit var data: ReserveSlots;
    private lateinit var data1: DroPassengersModel;
    lateinit var binding: FragmentVerifyVehicleBinding
    private lateinit var  Status: String;
    val args: VerifyVehicleArgs by navArgs()
    private lateinit var docId: String;
    private lateinit var slotId: String;
    private lateinit var Usercode: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verify_vehicle, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)



        binding = FragmentVerifyVehicleBinding.bind(view)

          binding.Name.setText(args.name)
          binding.slotName.setText(args.slotname)
          binding.plateNo.setText(args.platenumber)
          binding.description.setText(args.description)
            Status = args.status.toString()
            slotId = args.slotid.toString()
            Usercode = args.usercode.toString()


        binding.approve.setOnClickListener {

          CoroutineScope(Dispatchers.IO).launch {

              if(args.calltype.equals("DROPPING"))
              {
                  GetDroppingTransactionDetails(view,args.calltype.toString(),args.usercode.toString())

              }else {

                  GetTransactionDetails(view,args.calltype.toString(),args.usercode.toString())
              }
          }

        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                CoroutineScope(Dispatchers.IO).launch {

                    if(args.calltype.equals("DROPPING"))
                    {
                        GetDroppingTransactionDetails(view,"DECLINED",args.usercode.toString())

                    }else {

                        GetTransactionDetails(view,"DECLINED",args.usercode.toString())
                    }
                }

            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

    }




    @RequiresApi(Build.VERSION_CODES.N)
    private suspend fun GetTransactionDetails(view: View, action: String, user: String){

        try {

            val docRef = firestoreDB.collection(FirebaseInstance.ReserveSlots)
                .whereEqualTo("userCode", user)
                .whereIn("status",listOf("PENDING", "OCCUPIED"))
                .limit(1)
                .get().await()


            if (docRef.size() == 0 || docRef.isEmpty)
            {

                withContext(Dispatchers.Main){

                    ToastMessage("No reserved slots found!", "INFO")
                    Navigation.findNavController(view).navigate(R.id.action_QRCodeScanner_to_reservedParkingSpaces)

                }

            }else {

                for (document in docRef.getDocuments()) {
                    data = document.toObject(ReserveSlots::class.java)!!
                    docId = document.getId().toString();

                }

            lateinit  var stats: String;
                lateinit var transaction: String;
                lateinit var timeIn: String;
                lateinit var timeOut: String;

                val currentTimeNow = Calendar.getInstance()
                println("Current time now : " + currentTimeNow.time)
                val simpleDateFormat = SimpleDateFormat(" hh:mm a")
                val  dateTime = simpleDateFormat.format(currentTimeNow.time)

                if(Status.equals("PENDING")){

                    if(action.equals("DECLINED")){

                        stats = "DECLINED";
                        transaction = "NONE";
                       timeIn =   ""


                    } else {

                        stats = "OCCUPIED";
                        transaction = "PARKED";
                        timeIn =  dateTime


                    }

                    firestoreDB.collection(FirebaseInstance.ParkingLots).document(slotId)
                        .update("Status",stats).await()

                    firestoreDB.collection(FirebaseInstance.UserProfile).document(user)
                        .update(mapOf(
                            "Transaction" to transaction
                        )).await()

                    firestoreDB.collection(FirebaseInstance.ReserveSlots).document(docId)
                        .update(mapOf(
                            "status" to stats,
                            "timeIn" to timeIn
                        )).await()


                }  else{

                    stats = "SIGN_OUT"
                    timeOut =  dateTime


                    firestoreDB.collection(FirebaseInstance.ParkingLots).document(slotId)
                        .update("Status","VACANT").await()

                    firestoreDB.collection(FirebaseInstance.UserProfile).document(user)
                        .update(mapOf(
                            "Transaction" to "NONE",
                            "Rating" to "NONE"
                        )).await()

                    firestoreDB.collection(FirebaseInstance.ReserveSlots).document(docId)
                        .update(mapOf(
                            "status" to stats,
                            "timeOut" to timeOut,
                        )).await()



                }



                withContext(Dispatchers.Main){

                    ToastMessage("Transaction Success", "SUCCESS")
                     Navigation.findNavController(view).navigate(R.id.action_verifyVehicle_to_reservedParkingSpaces)


                }

            }


        } catch (e: Exception) {

            Log.d(FirebaseAnalytics.Event.SIGN_UP, e.localizedMessage)

            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")
            }


        }
    }


    private suspend fun GetDroppingTransactionDetails(view: View, action: String, user: String){

        try {

            val docRef = firestoreDB.collection(FirebaseInstance.DroppingPassenger)
                .document(user)
                .get().await()

            if (!docRef.exists())
            {

                withContext(Dispatchers.Main){

                    ToastMessage("No reserved slots found!", "INFO")
                    Navigation.findNavController(view).navigate(R.id.action_QRCodeScanner_to_dropPassengerList)

                }

            }else {



                var stats: String;
                var transaction: String

                if(docRef["status"].toString().equals("PENDING")){

                    if(action.equals("DECLINED")){

                        stats = "DECLINED";
                        transaction = "NONE";
                    }
                    else{

                        stats = "UNLOADING";
                        transaction = "UNLOADING";

                    }

                    //update status sa booking
                  firestoreDB.collection(FirebaseInstance.DroppingPassenger).document(user)
                       .update("status",stats).await()

                    //update transaction sa userprofile
                    firestoreDB.collection(FirebaseInstance.UserProfile).document(user)
                        .update("Transaction",transaction).await()


                }  else{

                    stats = "SIGN_OUT"
                    val timestamp = docRef["bookedDate"] as Timestamp
                    val rating: String = getRating(timestamp)

                    firestoreDB.collection(FirebaseInstance.DroppingPassenger).document(user)
                        .update("status",stats).await()

                    firestoreDB.collection(FirebaseInstance.UserProfile).document(user)
                        .update(mapOf(
                            "Transaction" to "NONE",
                            "Rating" to rating
                        )).await()
                }




                withContext(Dispatchers.Main){

                    ToastMessage("Transaction Success", "SUCCESS")
                    Navigation.findNavController(view).navigate(R.id.action_verifyVehicle_to_dropPassengerList)


                }




            }


        } catch (e: Exception) {



            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")
            }


        }
    }


    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","INFO"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }


    private fun getRating(bookedDate: Timestamp): String {


       var date1 = bookedDate.toDate()?.getTime()

        var date2 = Timestamp.now().toDate()?.getTime()

        val diff: Long = date2 - date1!!
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60


       val rating =  if(seconds > 10) "UNLOADING OVERTIME" else "NO PENALTY"
        return rating;
    }

}