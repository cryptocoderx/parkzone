package com.example.parkzone

import android.annotation.SuppressLint
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.compose.ui.input.key.Key.Companion.D
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.Navigation
import com.example.parkzone.databinding.BookdialogBinding
import com.example.parkzone.databinding.DropdownItemBinding
import com.example.parkzone.databinding.FragmentDropPassengerBinding
import com.google.android.gms.location.*
import com.google.firebase.Timestamp
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.concurrent.TimeUnit






class DropPassenger : Fragment() {

    private lateinit var user_type: String;

    private lateinit var binding: FragmentDropPassengerBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_drop_passenger, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    @SuppressLint("MissingPermission")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentDropPassengerBinding.bind(view);


        binding.submitBook.setOnClickListener {


            val   plateNo = binding.plateNo.text.toString().trim { it<= ' '  } // trim fist and last space
            val  description = binding.description.text.toString().trim { it<= ' '  }


            if(plateNo.isEmpty())
            {
                ToastMessage("Plate number is required", "WARNING")
                return@setOnClickListener

            }

            if(description.isEmpty())
            {
                ToastMessage("Vehicle description is required", "WARNING")
                return@setOnClickListener

            }

            ToastMessage("Loading...", "WARNING")
            CoroutineScope(Dispatchers.IO).launch {

                val document = FirebaseInstance.firestoreDB.collection("UserProfile").
                document(FirebaseInstance.firebaseAuth.currentUser!!.uid).get().await()

                val  data = document.toObject(UserProfile::class.java)

                if (data!!.UserType == 1)

                { user_type= "Employee"

                } else {user_type= "Student"

                }


                val currentTimeNow = Calendar.getInstance()
                println("Current time now : " + currentTimeNow.time)
                currentTimeNow.add(Calendar.MINUTE, 30)
                val simpleDateFormat = SimpleDateFormat(" hh:mm a")
                 val  dateTime = simpleDateFormat.format(currentTimeNow.time)

                val reserveSlots = DroPassengersModel(
                    FirebaseInstance.firebaseAuth.currentUser!!.uid,
                    plateNo,
                    description,
                    data.IdNumber,
                    data.Firstname.plus(" "+ data.Lastname),
                    user_type,
                    Timestamp.now(),
                    "PENDING",
                    dateTime
                )

                bookingProcess(view, reserveSlots);

            }
        }

        binding.cancelbook.setOnClickListener {

            Navigation.findNavController(view).navigate(R.id.action_dropPassenger_to_mainFragment)

        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                Navigation.findNavController(view).navigate(R.id.action_dropPassenger_to_mainFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

    }


    private suspend fun bookingProcess(view: View, reserveSlots: DroPassengersModel){

        try {

            FirebaseInstance.firestoreDB.collection(FirebaseInstance.DroppingPassenger)
                .document(FirebaseInstance.firebaseAuth.currentUser!!.uid)
                .set(reserveSlots).await()

            FirebaseInstance.firestoreDB.collection(FirebaseInstance.UserProfile).document(
                FirebaseInstance.firebaseAuth.currentUser!!.uid)
                .update("Transaction","PENDING").await()

            withContext(Dispatchers.Main){

                ToastMessage("Transaction Success", "SUCCESS")
                Navigation.findNavController(view).navigate(R.id.action_dropPassenger_to_mainFragment)

            }


        } catch (e: Exception) {

            Log.d("BOOKING", e.localizedMessage)

            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")

            }


        }
    }

    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","WARNING"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }




}