package com.example.parkzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.MainThread
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.parkzone.Adapters.ParkingLotsAdapter
import com.example.parkzone.FirebaseInstance.Companion.firebaseAuth
import com.example.parkzone.databinding.FragmentMainBinding
import com.example.parkzone.databinding.FragmentParkingLotListBinding
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class ParkingListFragment : Fragment() {

    private  lateinit var binding: FragmentParkingLotListBinding
    private lateinit var adapter: ParkingLotsAdapter
    private  lateinit  var transaction: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment



      val view: View = inflater.inflate(R.layout.fragment_parking_lot_list, container, false)


        binding =  FragmentParkingLotListBinding.bind(view);
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.setHasFixedSize(true)

        return view;
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        TransactionListener()

        adapter = ParkingLotsAdapter(
            ParkingLotsAdapter.OnClickListener { data ->
                activity?.let {

                    if(transaction.equals("PENDING")){
                        ToastMessage("You already have a pending transaction","INFO")
                    }else{
                        BookDialog.newInstance(data.Id.toString(),data.SlotName.toString()
                            ,data.Latitude.toString(),data.Longitude.toString()).
                        show(it.supportFragmentManager, "SimpleDialog")
                    }


              }
            }
            )


     CoroutineScope(Dispatchers.IO).launch{

         val document = FirebaseInstance.firestoreDB.collection("UserProfile").
         document(firebaseAuth.currentUser!!.uid).get().await()

         val  data = document.toObject(UserProfile::class.java)

           withContext(Dispatchers.Main){

               Log.d("DEP",data?.Department.toString().trim())
               getParkingLots(data?.Department.toString())

           }

     }


        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view).navigate(R.id.action_parkingLotList_to_mainFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

    }


    private fun getParkingLots(department: String){

        val documentsRef  =  FirebaseInstance.firestoreDB.collection(FirebaseInstance.ParkingLots)
            .whereEqualTo("Department", department)
            .whereEqualTo("Status", "VACANT")

        documentsRef.addSnapshotListener(EventListener { documentSnapshots, e ->
            if (e != null) {
                Log.e("DOCS", "Listen failed!", e)
                return@EventListener
            }

            val data =  documentSnapshots!!.toObjects(ParkingLots::class.java);

            adapter.submitList(data)
            binding.recyclerView.adapter = adapter;
        })
    }



    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","INFO"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }

    private fun TransactionListener(){
        val docRefProfile =  FirebaseInstance.firestoreDB.collection(FirebaseInstance.UserProfile)
            .document(firebaseAuth.currentUser!!.uid)
        docRefProfile.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(BookDialog.TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null) {

                val Transaction: String = snapshot.data!!["Transaction"].toString()

                if(Transaction.equals("PENDING"))
                {

                    transaction = "PENDING"

                }else{

                   transaction = Transaction
                }
            }else{

                ToastMessage("Unable to locate user account","INFO")
            }

        }
    }

}