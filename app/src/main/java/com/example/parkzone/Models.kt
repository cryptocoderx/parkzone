package com.example.parkzone

import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id


data class UserProfile
    (var IdNumber: String? = null,
     var Firstname: String?  = null,
     var Lastname: String? = null,
     var Department: String? = null,
     var Email: String? = null,
     var Password: String? = null,
     var PhoneNumber: String? = null,
     var UserType: Int? = null,
     var Rating: String? = null,
     var Transaction: String? = null)

data class ParkingLots
 (var Id: String? = null,
  var Department: String? = null,
  var Latitude: String? =  null,
  var Longitude: String? = null,
  var SlotDescription: String? = null,
  var SlotName: String? = null,
  var Status: String? = null)

data class ReserveSlots(
 var UserCode: String? = null,
 var PlateNumber: String? = null,
 var VehicleDescription: String? = null,
 var SlotId: String? = null,
 var SlotName: String? = null,
 var IDnumber: String? = null,
 var Name: String? = null,
 var UserType: String? = null,
 var Lat: String? = null,
 var Long: String? =null,
 var BookedDate:  Timestamp? =null,
 var Status: String? =null,
 var TimeIn: String? =null,
 var TimeOut: String? =null,
  )



data class DroPassengersModel(
 var UserCode: String? = null,
 var PlateNumber: String? = null,
 var VehicleDescription: String? = null,
 val idnumber: String? = null,
 var Name: String? = null,
 var UserType: String? = null,
 var BookedDate:  Timestamp? =null,
 var Status: String? =null,
 var TimeOut: String? =null,
 var Lat: String? = null,
 var Long: String? =null,
)

@Entity
data class User(
 @Id
 var id: Long = 0,
 var UserCode: String? = null,
 var IDnumber: String? = null,
)


