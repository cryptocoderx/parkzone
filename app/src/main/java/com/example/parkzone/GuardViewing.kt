package com.example.parkzone

import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.parkzone.databinding.FragmentGuardViewingBinding


class GuardViewing : Fragment() {

      lateinit var binding: FragmentGuardViewingBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
      val view: View = inflater.inflate(R.layout.fragment_guard_viewing, container, false)

        binding = FragmentGuardViewingBinding.bind(view);




        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val  notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        var player =  MediaPlayer.create(requireActivity().applicationContext, notification)

        binding.listCard1.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_guardViewing_to_availableParkingSpace)
        }

        binding.listCard2.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_guardViewing_to_reservedParkingSpaces)
        }

        binding.listCard3.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_guardViewing_to_dropPassengerList)
        }


        val documentsRef  =  FirebaseInstance.firestoreDB.collection(FirebaseInstance.DroppingPassenger)
            .whereEqualTo("status","TIMES-UP")

        documentsRef.addSnapshotListener { snapshot, e ->

            if (e != null) {
                Log.w(BookDialog.TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            val data =  snapshot!!.toObjects(DroPassengersModel::class.java);




            if(data.size > 0){

                player =  MediaPlayer.create(requireActivity().applicationContext, notification)

                player.setLooping(true)
                player.start()

            }else{

                if(player.isPlaying){
                    player.stop();
                    player.reset()
                    player.release()
                }
                Log.d("SIZE",data.size.toString())
            }



        }

    }
}