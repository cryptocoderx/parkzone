package com.example.parkzone

import android.content.Intent
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.parkzone.Adapters.DroppingListAdapter
import com.example.parkzone.Adapters.ReserveSlotsAdapter
import com.example.parkzone.databinding.FragmentDropPassengerListBinding
import com.example.parkzone.databinding.FragmentReservedParkingSpacesBinding
import com.google.firebase.firestore.EventListener


class DropPassengerList : Fragment() {


    lateinit var binding: FragmentDropPassengerListBinding
    private lateinit var adapter: DroppingListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View =     inflater.inflate(R.layout.fragment_drop_passenger_list, container, false)

        binding = FragmentDropPassengerListBinding.bind(view)
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.setHasFixedSize(true)


        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = DroppingListAdapter(requireContext(),
            DroppingListAdapter.OnClickListener { data ->
                activity?.let {

                    val gmmIntentUri: Uri = Uri.parse("google.navigation:q="+data.Lat.toString()+","+data.Long.toString())
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    startActivity(mapIntent)
                    //   BookDialog.newInstance(data.Id.toString(),data.SlotName.toString(),data.Latitude.toString(),data.Longitude.toString()).show(it.supportFragmentManager, "SimpleDialog")
                }
            }
        )

        val documentsRef  =  FirebaseInstance.firestoreDB.collection(FirebaseInstance.DroppingPassenger)
            .whereIn("status",listOf("PENDING","UNLOADING","TIMES-UP"))

        documentsRef.addSnapshotListener(EventListener { documentSnapshots, e ->
            if (e != null) {
                Log.e("DOCS", "Listen failed!", e)
                return@EventListener
            }

            val data =  documentSnapshots!!.toObjects(DroPassengersModel::class.java);



            /*   val notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
               val player: MediaPlayer = MediaPlayer.create(context, notification)
               player.isLooping = true
               player.start() */



            adapter.submitList(data)
            binding.recyclerView.adapter = adapter;
        })


        binding.scanbtn.setOnClickListener {
            val action = DropPassengerListDirections.actionDropPassengerListToQRCodeScanner("DROPPING")
            Navigation.findNavController(view).navigate(action)

        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                    Navigation.findNavController(view).navigate(R.id.action_dropPassengerList_to_guardViewing)

            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}