package com.example.parkzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.parkzone.Adapters.ParkingLotsAdapter
import com.example.parkzone.Adapters.ReserveSlotsAdapter
import com.example.parkzone.databinding.FragmentReservedParkingSpacesBinding
import com.google.firebase.firestore.EventListener


class ReservedParkingSpaces : Fragment() {

    lateinit var binding: FragmentReservedParkingSpacesBinding
    private lateinit var adapter: ReserveSlotsAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
val view: View =  inflater.inflate(R.layout.fragment_reserved_parking_spaces, container, false)

           binding = FragmentReservedParkingSpacesBinding.bind(view)
           binding.recyclerView.layoutManager = LinearLayoutManager(activity)
          binding.recyclerView.setHasFixedSize(true)


        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)


        adapter = ReserveSlotsAdapter(
            ReserveSlotsAdapter.OnClickListener { data ->
                activity?.let {

                    //   BookDialog.newInstance(data.Id.toString(),data.SlotName.toString(),data.Latitude.toString(),data.Longitude.toString()).show(it.supportFragmentManager, "SimpleDialog")
                }
            }
        )

        val documentsRef  =  FirebaseInstance.firestoreDB.collection(FirebaseInstance.ReserveSlots)
            .whereIn("status",listOf("PENDING", "OCCUPIED"))

        documentsRef.addSnapshotListener(EventListener { documentSnapshots, e ->
            if (e != null) {
                Log.e("DOCS", "Listen failed!", e)
                return@EventListener
            }

            val data =  documentSnapshots!!.toObjects(ReserveSlots::class.java);
            adapter.submitList(data)
            binding.recyclerView.adapter = adapter;
        })


        binding.scanbtn.setOnClickListener {
            val action = ReservedParkingSpacesDirections.actionReservedParkingSpacesToQRCodeScanner("PARKING")
            Navigation.findNavController(view).navigate(action)

        }


        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view)
                    .navigate(R.id.action_reservedParkingSpaces_to_guardViewing)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}