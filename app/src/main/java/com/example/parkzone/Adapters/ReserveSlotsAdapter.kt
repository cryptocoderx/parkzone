package com.example.parkzone.Adapters

import android.icu.util.Calendar
import android.os.Build
import android.text.format.DateFormat.format
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.parkzone.ParkingLots
import com.example.parkzone.R
import com.example.parkzone.ReserveSlots
import com.google.firebase.Timestamp
import java.text.DateFormat
import java.util.*

class ReserveSlotsAdapter(private val onClickListener: ReserveSlotsAdapter.OnClickListener) :
    ListAdapter<ReserveSlots, ReserveSlotsAdapter.MyViewHolder>(ReserveSlotsAdapter){


    companion object MyDiffUtil : DiffUtil.ItemCallback<ReserveSlots>() {
        override fun areItemsTheSame(oldItem: ReserveSlots, newItem:ReserveSlots): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ReserveSlots, newItem: ReserveSlots): Boolean {
            return oldItem.UserCode == newItem.UserCode
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.reserved_slots_item,parent,false)


        return MyViewHolder(itemView)

    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val slot: ReserveSlots = getItem(position)
        holder.clientName.text =slot.Name
        holder.status.text =  "Status: "+slot.Status
        holder.plateNo.text =  "PlateNo.: "+slot.PlateNumber
        holder.slotName.text =  "Slot: "+slot.SlotName
        holder.vehicleDesc.text = "Vechile Description: "+slot.VehicleDescription

         var date1 = slot.BookedDate?.toDate()?.getTime()

        var date2 = Timestamp.now().toDate()?.getTime()

        val diff: Long = date2 - date1!!
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60



        Log.d("TIME",minutes.toString())

        holder.itemView.setOnClickListener {
            onClickListener.onClick(slot)
        }

    }




    class MyViewHolder  (itemView: View) : RecyclerView.ViewHolder(itemView){

        val clientName = itemView.findViewById<TextView>(R.id.clientName)
        val status = itemView.findViewById<TextView>(R.id.status)
        val plateNo = itemView.findViewById<TextView>(R.id.plateNo)
        val slotName = itemView.findViewById<TextView>(R.id.slotName)
        val vehicleDesc = itemView.findViewById<TextView>(R.id.vehicleDesc)

    }

    class OnClickListener(

        val clickListener: (reserveSlots: ReserveSlots) -> Unit) {

        fun onClick(reserveSlots: ReserveSlots) = clickListener(reserveSlots)

    }

}