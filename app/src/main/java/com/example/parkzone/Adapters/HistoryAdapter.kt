package com.example.parkzone.Adapters

import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.parkzone.ParkingLots
import com.example.parkzone.R
import com.example.parkzone.ReserveSlots
import com.google.firebase.Timestamp

class HistoryAdapter(private val onClickListener:OnClickListener) :
    ListAdapter<ReserveSlots, HistoryAdapter.MyViewHolder>(HistoryAdapter){


    companion object MyDiffUtil : DiffUtil.ItemCallback<ReserveSlots>() {
        override fun areItemsTheSame(oldItem: ReserveSlots, newItem: ReserveSlots): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ReserveSlots, newItem: ReserveSlots): Boolean {
            return oldItem.UserCode == newItem.UserCode
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.transactions_item,parent,false)


        return MyViewHolder(itemView)

    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val slot: ReserveSlots = getItem(position)

        holder.vehicleDesc.text =  "Used Vehicle.: "+slot.VehicleDescription
        holder.slotName.text =  "Slot: "+slot.SlotName
        holder.dateBooked.text = slot.BookedDate?.toDate().toString()


        holder.itemView.setOnClickListener {
            onClickListener.onClick(slot)
        }

    }




    class MyViewHolder  (itemView: View) : RecyclerView.ViewHolder(itemView){


        val slotName = itemView.findViewById<TextView>(R.id.parkingName)
        val vehicleDesc = itemView.findViewById<TextView>(R.id.vehicleDesc)
        val dateBooked = itemView.findViewById<TextView>(R.id.dateBooked)

    }

    class OnClickListener(

        val clickListener: (reserveSlots: ReserveSlots) -> Unit) {

        fun onClick(reserveSlots: ReserveSlots) = clickListener(reserveSlots)

    }

}