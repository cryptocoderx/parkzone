package com.example.parkzone.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.parkzone.ParkingLots
import com.example.parkzone.R
import org.w3c.dom.Text

class ParkingLotsAdapter(private val onClickListener: OnClickListener) :
    ListAdapter<ParkingLots, ParkingLotsAdapter.MyViewHolder>(MyDiffUtil) {


    companion object MyDiffUtil : DiffUtil.ItemCallback<ParkingLots>() {
        override fun areItemsTheSame(oldItem: ParkingLots, newItem: ParkingLots): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: ParkingLots, newItem: ParkingLots): Boolean {
            return oldItem.Id == newItem.Id
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParkingLotsAdapter.MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.parkinglots_item,parent,false)


        return MyViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: ParkingLotsAdapter.MyViewHolder, position: Int) {
        val slot: ParkingLots = getItem(position)
        holder.parkingName.text = slot.SlotName;
        holder.parkingDesc.text = slot.SlotDescription

        holder.itemView.setOnClickListener {
            onClickListener.onClick(slot)
        }

    }




    class MyViewHolder  (itemView: View) :RecyclerView.ViewHolder(itemView){

        val parkingName = itemView.findViewById<TextView>(R.id.parkingName)
        val parkingDesc = itemView.findViewById<TextView>(R.id.parkingDesc)
    }

    class OnClickListener(

        val clickListener: (parkinglot: ParkingLots) -> Unit) {

        fun onClick(parkinglot: ParkingLots) = clickListener(parkinglot)

    }


}