package com.example.parkzone.Adapters

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.parkzone.DroPassengersModel
import com.example.parkzone.R
import com.example.parkzone.ReserveSlots
import com.google.firebase.Timestamp
import android.media.RingtoneManager
import android.net.Uri
import android.media.MediaPlayer





class DroppingListAdapter(val context: Context ,private val onClickListener: DroppingListAdapter.OnClickListener) :
    ListAdapter<DroPassengersModel,  DroppingListAdapter.MyViewHolder>( DroppingListAdapter){


    companion object MyDiffUtil : DiffUtil.ItemCallback<DroPassengersModel>() {
        override fun areItemsTheSame(oldItem: DroPassengersModel, newItem: DroPassengersModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: DroPassengersModel, newItem: DroPassengersModel): Boolean {
            return oldItem.UserCode == newItem.UserCode
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.reserved_slots_item,parent,false)


        return MyViewHolder(itemView)

    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val slot: DroPassengersModel= getItem(position)

            lateinit var stats: String
            if(slot.Status.equals("TIMES-UP"))
            {
                stats = "TIMES-UP"
            }else{

                stats = slot.Status.toString()
            }

        holder.clientName.text =slot.Name
        holder.status.text =  slot.Status.toString()
        holder.plateNo.text =  "PlateNo.: "+slot.PlateNumber
        holder.slotName.text =  "EndTime: "+ slot.TimeOut
        holder.vehicleDesc.text = "Vechile Description: "+slot.VehicleDescription






        holder.itemView.setOnClickListener {
            onClickListener.onClick(slot)
        }

        /*
        val alarmManager =  context.getSystemService(ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, MyAlarm::class.java)
        val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0)
        alarmManager.setRepeating(
            AlarmManager.RTC,
            Timestamp.now().toDate().time,
            AlarmManager.INTERVAL_DAY,
            pendingIntent
        )*/





    }




    class MyViewHolder  (itemView: View) : RecyclerView.ViewHolder(itemView){

        val clientName = itemView.findViewById<TextView>(R.id.clientName)
        val status = itemView.findViewById<TextView>(R.id.status)
        val plateNo = itemView.findViewById<TextView>(R.id.plateNo)
        val slotName = itemView.findViewById<TextView>(R.id.slotName)
        val vehicleDesc = itemView.findViewById<TextView>(R.id.vehicleDesc)

    }

    class OnClickListener(

        val clickListener: (droPassengersModel: DroPassengersModel) -> Unit) {

        fun onClick(droPassengersModel: DroPassengersModel) = clickListener(droPassengersModel)

    }



    private class MyAlarm : BroadcastReceiver() {
        override fun onReceive(
            context: Context,
            intent: Intent
        ) {
            Log.d("Alarm Bell", "Alarm just fired")
        }
    }

    private fun setAlarm(timeInMillis: Long, holder: RecyclerView.ViewHolder) {

    }


}