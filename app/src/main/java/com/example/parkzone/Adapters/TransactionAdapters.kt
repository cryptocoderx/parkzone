package com.example.parkzone.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.parkzone.ParkingLots
import com.example.parkzone.R
import com.example.parkzone.ReserveSlots

class TransactionAdapters(private val onClickListener: OnClickListener) :
    ListAdapter<ReserveSlots, TransactionAdapters.MyViewHolder>(MyDiffUtil) {


    companion object MyDiffUtil : DiffUtil.ItemCallback<ReserveSlots>() {
        override fun areItemsTheSame(oldItem: ReserveSlots, newItem: ReserveSlots): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem:  ReserveSlots, newItem: ReserveSlots): Boolean {
            return oldItem.IDnumber == newItem.IDnumber
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.transactions_item,parent,false)


        return MyViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val slot: ReserveSlots = getItem(position)

        var timeInlogs: String
        var timeOutLogs: String;

        if(slot.TimeIn.isNullOrBlank() ||slot.TimeIn.isNullOrEmpty())
        {
            timeInlogs = "NO LOGS"
        }else{
            timeInlogs = slot.TimeIn.toString()

        }

        if(slot.TimeOut.isNullOrBlank()||slot.TimeOut.isNullOrEmpty())
        {
           timeOutLogs = "NO LOGS"
        }else{

            timeOutLogs = slot.TimeOut.toString()
        }


        holder.parkingName.text = slot.SlotName;
        holder.parkingDesc.text = "Vehicle Desc:"+slot.VehicleDescription
        holder.dateBooked.text = slot.BookedDate?.toDate().toString()
        holder.status.text = slot.Status
        holder.timeIn_tv.text = "TimeIn: "+timeInlogs
        holder.timeOut_tv.text = "TimeOut: "+timeOutLogs

        holder.itemView.setOnClickListener {
            onClickListener.onClick(slot)
        }

    }




    class MyViewHolder  (itemView: View) : RecyclerView.ViewHolder(itemView){

        val parkingName = itemView.findViewById<TextView>(R.id.parkingName)
        val parkingDesc = itemView.findViewById<TextView>(R.id.parkingDesc)
        val dateBooked = itemView.findViewById<TextView>(R.id.dateBooked)
        val status = itemView.findViewById<TextView>(R.id.status)
        val timeIn_tv = itemView.findViewById<TextView>(R.id.timeInlogs)
        val timeOut_tv = itemView.findViewById<TextView>(R.id.timeOutlogs)
    }

    class OnClickListener(

        val clickListener: (parkinglot: ReserveSlots) -> Unit) {

        fun onClick(parkinglot: ReserveSlots) = clickListener(parkinglot)

    }


}
