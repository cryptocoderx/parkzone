package com.example.parkzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.navigation.Navigation
import com.example.parkzone.databinding.ActivityMainBinding
import com.example.parkzone.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit  var email: String;
    private lateinit  var password: String;
    private lateinit var action: LoginFragmentDirections;
    private lateinit var auth: FirebaseAuth
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = Firebase.auth

        binding = FragmentLoginBinding.bind(view)

        binding.signInBtn.setOnClickListener{

            email = binding.emailedt.text.toString();
            password = binding.passwordedt.text.toString();

           if((email.isEmpty() && email.isBlank()))
            {

                FancyToast.makeText(activity,"Email is required",FancyToast.LENGTH_LONG,FancyToast.INFO,true).show();
                return@setOnClickListener
            }

            if((password.isEmpty() && password.isBlank())){

                FancyToast.makeText(activity,"Password is required !",FancyToast.LENGTH_LONG,FancyToast.INFO,true).show();
                return@setOnClickListener
            }



            ToastMessage("Loading...", "LOADING")

            if(email.equals("GUARD") && password.equals("GUARD")){

                Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_guardViewing)

            }else{

            CoroutineScope(Dispatchers.IO).launch{

                //LoginProcess(view,email,password);
                LoginProcess(view,email,password);
            }
            }


        }

        binding.signUpTv.setOnClickListener {



            Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_signUpFragment)
        }

    }

    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","WARNING"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }


    private suspend fun LoginProcess(view: View, email: String , password: String){

        try {

            auth.signInWithEmailAndPassword(email, password).await()
            var  user = auth.currentUser;
            withContext(Dispatchers.Main){


                if(user!!.isEmailVerified){
                    ToastMessage("Login Success", "SUCCESS")
                    Navigation.findNavController(view).navigate(R.id.action_loginFragment_to_mainFragment);

                }else{
                    ToastMessage("Please verify your email!", "WARNING")

                }


            }


        } catch (e: Exception) {


            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")
            }


        }
    }
}