package com.example.parkzone

import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class FirebaseInstance {

    companion object{

        //REFERENCE
         val  firebaseAuth = Firebase.auth

        val   firestoreDB = FirebaseFirestore.getInstance()


        //COLLECTIONS
        val UserProfile = "UserProfile";

        val ParkingLots = "ParkingLots";

        val ReserveSlots = "ReserveSlots";

        val OutSourceData = "OutSourceData";


        val DroppingPassenger = "DroppingPassengers";


    }

}