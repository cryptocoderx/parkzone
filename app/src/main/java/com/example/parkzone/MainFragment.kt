package com.example.parkzone

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.work.*
import com.example.parkzone.BookDialog.Companion.TAG
import com.example.parkzone.FirebaseInstance.Companion.DroppingPassenger
import com.example.parkzone.FirebaseInstance.Companion.firebaseAuth
import com.example.parkzone.FirebaseInstance.Companion.firestoreDB
import com.example.parkzone.databinding.ActivityMainBinding
import com.example.parkzone.databinding.FragmentMainBinding
import com.google.android.gms.location.*
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.Query
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter

import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import javax.annotation.meta.When
import kotlin.properties.Delegates


class MainFragment : Fragment() {


    private lateinit var binding: FragmentMainBinding
    private lateinit var reserveData: ReserveSlots

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var currentLocation: Location? = null

    private  var transaction: String = "NO_PENDING"
    private lateinit var rating: String;
    private lateinit var usertype: String


    private  var timelimit : Long = 0

    companion object{
        const val KEY_COUNT_VALUE = "key_count"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentMainBinding.bind(view)
        setDataToQrCode();

     //   setOneTimeWorkRequest();

        val docRef =  firestoreDB.collection(DroppingPassenger)
            .document(firebaseAuth.currentUser!!.uid)
        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {


                if(snapshot.data!!["status"].toString().equals("UNLOADING"))
                {

                    timelimit = 1000*10;
                   countDownTime.start()

                } else if(snapshot.data!!["status"].toString().equals("TIMES-UP"))
                {

                    binding.subtitle.text =   "Times-Up, Please leave the campus."

                }


            } else {


                //binding.subtitle.text = "Please park properly."
                Log.d("sdsds", "Current data: null")


            }
        }


        val docRefProfile =  firestoreDB.collection(FirebaseInstance.UserProfile)
            .document(firebaseAuth.currentUser!!.uid)
        docRefProfile.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null) {

                val Transaction: String = snapshot.data!!["Transaction"].toString()

                rating = snapshot.data!!["Rating"].toString()
                usertype = snapshot.data!!["UserType"].toString()

                binding.name.text = snapshot.data!!["Firstname"].toString()

                if(Transaction.equals("NONE")||Transaction.equals("None"))
                {
                    transaction = "NONE"
                    binding.subtitle.text = "Please park properly."
                }
                else if(Transaction.equals("PENDING"))
                {
                    transaction = "PENDING";
                    binding.subtitle.text = "Request pending for approval."

                }else if(Transaction.equals("PENDING_BOOKING"))
                {
                    countDownTime1.start()

                }
                else if(Transaction.equals("PARKED"))
                {
                    transaction = "PENDING"
                    binding.subtitle.text = "Request approved, park properly."
                }

            }else{

                ToastMessage("Unable to locate user account","INFO")
            }

        }




        binding.reservedParkingCard.setOnClickListener {


            if(transaction.equals("PENDING")  || transaction.equals("PENDING_BOOKING")) {

                basicAlert(view,"Unable to proceed, you currently have a pending transaction.","PENDING REQUEST")

            } else if(rating.equals("UNLOADING OVERTIME")) {

                basicAlert(view,"Your account is currently restricted due to violation, pls contact administrator.","WARNING")

            }else{
                Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_parkingLotList);

            }


        }


        binding.dropParkingCard.setOnClickListener{

            if(transaction.equals("PENDING")) {

                basicAlert(view,"Unable to proceed, you currently have a pending transaction.","PENDING REQUEST")

            } else if(rating.equals("UNLOADING OVERTIME")) {

                basicAlert(view,"Your account is currently restricted due to violation, pls contact administrator.","WARNING")

            }else if(!usertype.equals("1")){

                basicAlert(view,"You're not allowed to use these feature.","RESTRICTED")

            } else {
                Navigation.findNavController(view)
                    .navigate(R.id.action_mainFragment_to_dropPassenger)

            }
        }



        binding.historyCard.setOnClickListener{

           Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_transactionsFragment)
        }


        binding.signOutBtn.setOnClickListener{

            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_loginFragment)

        }


        binding.profileCard.setOnClickListener{

            Navigation.findNavController(view).navigate(R.id.action_mainFragment_to_profileFragment)
        }

        binding.navigationCard.setOnClickListener{

            CoroutineScope(Dispatchers.IO).launch {

                getSlotCoordinates(view)
            }
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                ToastMessage("Please use the sign-out button","INFO")
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
   /*
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)

        val locationRequest = LocationRequest.create()?.apply {
            interval = TimeUnit.SECONDS.toMillis(1)
            fastestInterval = TimeUnit.SECONDS.toMillis(1)
            maxWaitTime = TimeUnit.MINUTES.toMillis(1)
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        locationCallback = object : LocationCallback() {

            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)

                currentLocation = locationResult.lastLocation

                Log.d("LOCATION",currentLocation?.latitude.toString())

            }
        }




        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())

     */
    }


    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","INFO"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }


    private suspend fun getSlotCoordinates(view: View){

        try {
            val document = firestoreDB.collection(FirebaseInstance.ReserveSlots)
                .whereEqualTo("userCode", firebaseAuth.currentUser!!.uid)
                .whereIn("status", listOf("OCCUPIED", "PENDING"))
                .limit(1)
                .get().await()


            if( document.isEmpty && document.size() == 0)
            {

                withContext(Dispatchers.Main){

                    ToastMessage("No reserved slot found, unable to navigate","INFO");

                }


            }else{




                for (document in document.getDocuments()) {
                    reserveData = document.toObject(ReserveSlots::class.java)!!
                }


                val lat = reserveData.Lat;
                val lon = reserveData.Long

                withContext(Dispatchers.Main){

                    val gmmIntentUri: Uri = Uri.parse("google.navigation:q="+lat+","+lon)
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    startActivity(mapIntent)

                }


            }
        }catch (e: Exception) {

            Log.d("GET_SLOT_AREA", e.localizedMessage)

            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")
            }


        }

    }


    private fun setDataToQrCode(){

        val writer = QRCodeWriter()
        val bitMatrix = writer.encode(firebaseAuth.currentUser!!.uid, BarcodeFormat.QR_CODE, 156, 156)
        val width = bitMatrix.width
        val height = bitMatrix.height
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }


        binding.qrCode.setImageBitmap(Bitmap.createScaledBitmap(
            bitmap,
            1000,
            1000,
            false
        ))

    }


    fun basicAlert(view: View, message: String, title: String){

        val dialogBuilder = AlertDialog.Builder(requireActivity())
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)
            .setPositiveButton("CLOSE", DialogInterface.OnClickListener {
                    dialog, id ->
                dialog.dismiss()

            })

        val alert = dialogBuilder.create()
        alert.setTitle("INFORMATION")
        alert.show()


    }



    override fun onStart() {
        super.onStart()

        if (ActivityCompat.checkSelfPermission(
                requireActivity().applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireActivity().applicationContext,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ToastMessage("Please turn on device permission", "ERROR")
            return
        }




    }


     var countDownTime = object : CountDownTimer(1000*20,1000){
            override fun onTick(millisUntilFinished: Long) {

                val timer: String = requireActivity().getString(R.string.formatted_time,
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60)


                    binding.subtitle.text =   "Remaining time to drop passenger: ${timer}"


                Log.d("TIME",timer)
            }

            @SuppressLint("MissingPermission")
            override fun onFinish() {


                    val  fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity)

                    fusedLocationClient.lastLocation.addOnSuccessListener {

                            location : Location? ->

                        if(location != null )
                        {
                            submitLocation(location.latitude.toString(), location.longitude.toString())
                        }else{

                            submitLocation("0","0")
                        }


                    }.addOnFailureListener {

                        submitLocation("0","0")

                    }

                }






        }


    var countDownTime1 = object : CountDownTimer(1000*20,1000){
        override fun onTick(millisUntilFinished: Long) {

            val timer: String = requireActivity().getString(R.string.formatted_time,
                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) % 60,
                TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) % 60)


            binding.subtitle.text =   "Remaining time to park: ${timer}"


            Log.d("TIME",timer)
        }

        @SuppressLint("MissingPermission")
        override fun onFinish() {
            binding.subtitle.text =   "Transaction Canceled"
                CoroutineScope(Dispatchers.IO).launch {

                    bookingCancel()
                }

        }


    }


    private suspend fun bookingCancel(){

        try {

            val docRef = firestoreDB.collection(FirebaseInstance.ReserveSlots)
                .whereEqualTo("userCode", firebaseAuth.currentUser!!.uid)
                .whereEqualTo("status", "PENDING")
                .limit(1)
                .get().await()

              firestoreDB.collection(FirebaseInstance.ReserveSlots).document(docRef.documents[0].id)
                  .update("status","CANCELED").await()

            firestoreDB.collection(FirebaseInstance.ParkingLots).document(docRef.documents[0]["slotId"].toString())
                .update("Status","VACANT").await()

            firestoreDB.collection(FirebaseInstance.UserProfile).document(firebaseAuth.currentUser!!.uid)
               .update("Transaction","NONE").await()


            withContext(Dispatchers.Main){

                binding.subtitle.text = "Transaction Canceled."
            }


        } catch (e: Exception) {

            Log.d("CANCEL_BOOKING", e.localizedMessage)


        }
    }



  private fun  submitLocation(lat: String, long: String){
      CoroutineScope(Dispatchers.Default).launch {

          try {

              firestoreDB.collection(FirebaseInstance.DroppingPassenger).document(
                  firebaseAuth.currentUser!!.uid)
                  .update(mapOf(
                      "status" to "TIMES-UP",
                      "lat" to lat,
                      "long" to long
                  )).await()

              withContext(Dispatchers.Main){

                  ToastMessage("Please leave now.", "INFO")

              }


          } catch (e: Exception) {

              Log.d("BOOKING", e.localizedMessage)

              withContext(Dispatchers.Main){
                  ToastMessage(e.localizedMessage, "ERROR")

              }


          }

      }

    }


    /*
    private fun setOneTimeWorkRequest() {

        val workManager = WorkManager.getInstance(requireActivity().applicationContext)


        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val data: Data = Data.Builder()
            .putInt(KEY_COUNT_VALUE,125)
            .build()
        val timerRequest = OneTimeWorkRequest.Builder(BackGroundTimer::class.java)
            .setConstraints(constraints)
            .setInputData(data)
            .build()

        workManager
            .enqueue(timerRequest)

        workManager.getWorkInfoByIdLiveData(timerRequest.id)
            .observe(viewLifecycleOwner, Observer {

                if(it.state.isFinished){
                    val data = it.outputData
                   // val message = data.getString(UploadWorker.KEY_WORKER)
                    Log.i(TAG,"dsdsds")
                }
            })


    }*/





}



