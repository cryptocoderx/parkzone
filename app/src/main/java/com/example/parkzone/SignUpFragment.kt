package com.example.parkzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.navigation.Navigation
import com.example.parkzone.databinding.FragmentSignUpBinding
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await

class SignUpFragment : Fragment() {


    private lateinit var binding: FragmentSignUpBinding
    private lateinit var  firstname: String;
    private lateinit var lastname: String;
    private lateinit var idNumber: String;
    private lateinit  var email: String;
    private lateinit  var password: String;
    private lateinit var  phone: String;
    private lateinit var auth: FirebaseAuth
    private val SIGN_UP = "SIGN_UP"
    private lateinit var firestoreDB: FirebaseFirestore;
    private lateinit var data: UserProfile;

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding =  FragmentSignUpBinding.inflate(inflater, container, false)
        return  binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentSignUpBinding.bind(view);
        firestoreDB = FirebaseFirestore.getInstance()


        auth = Firebase.auth

       binding.registerBtn.setOnClickListener {

           firstname = binding.firstname.text.toString().trim { it<= ' '  } // trim fist and last space
           lastname = binding.lastname.text.toString().trim { it<= ' '  }
           idNumber = binding.IdNumber.text.toString().trim { it<= ' '  }
           email = binding.email.text.toString().trim { it<= ' '  }
           password = binding.password.text.toString().trim { it<= ' '  }
           phone = binding.phone.text.toString().trim { it<= ' '  }

        when{

            firstname.isEmpty()->{
                   FancyToast.makeText(activity,"Firstname is required",
                   FancyToast.LENGTH_LONG,
                   FancyToast.INFO,true).show();}
               lastname.isEmpty()->{
                FancyToast.makeText(activity,"Lastname is required",
                    FancyToast.LENGTH_LONG,
                    FancyToast.INFO,true).show();}
              email.isEmpty()->{
                FancyToast.makeText(activity,"Email is required",
                    FancyToast.LENGTH_LONG,
                    FancyToast.INFO,true).show();}
            password.isEmpty()->{
                FancyToast.makeText(activity,"Password is required",
                    FancyToast.LENGTH_LONG,
                    FancyToast.INFO,true).show();}
            phone.isEmpty()->{
                FancyToast.makeText(activity,"Phone number is required",
                    FancyToast.LENGTH_LONG,
                    FancyToast.INFO,true).show();}

               else ->{



                var inputData: HashMap<String, Any>  =   hashMapOf(
                       "IdNumber" to  idNumber,
                       "Firstname" to   firstname,
                       "Lastname" to  lastname,
                       "Email" to email,
                       "Password" to  password,
                       "PhoneNumber" to phone
                   )

                   ToastMessage("Loading...", "LOADING")

                   CoroutineScope(Dispatchers.IO).launch {

                       SignUpProcess(view,inputData);

                   }
               }


           }





       }



        binding.signInText.setOnClickListener {

            Navigation.findNavController(view).navigate(R.id.action_signUpFragment_to_loginFragment)
        }



    }




    private suspend fun SignUpProcess(view: View, inputData: HashMap<String,Any>){

        try {

            val checkData = firestoreDB.collection(FirebaseInstance.OutSourceData)
                .whereEqualTo("IdNumber", inputData.get("IdNumber"))
                .whereEqualTo("Email", inputData.get("Email"))
                .limit(1)
                .get().await()

            if (checkData.size() == 0 || checkData.isEmpty)
            {

                 withContext(Dispatchers.Main){

                     ToastMessage("Only registered students and employees are allowed.", "INFO")
                 }

            }else {


                for (document in checkData.getDocuments()) {
                  data = document.toObject(UserProfile::class.java)!!
                }


                inputData.put("Department",data.Department.toString());
                inputData.put("UserType",Integer.parseInt(data.UserType.toString()));
                inputData.put("Rating","No Rating");
                inputData.put("Transaction","None");

                auth.createUserWithEmailAndPassword(inputData["Email"].toString(), inputData["Password"].toString()).await()
                auth.currentUser!!.sendEmailVerification().await()

                firestoreDB.collection(FirebaseInstance.UserProfile).document(auth.currentUser!!.uid).set(inputData).await()

                withContext(Dispatchers.Main) {

                    ToastMessage("Please verify your email to complete sign-up process.", "SUCCESS")
                    Navigation.findNavController(view)
                        .navigate(R.id.action_signUpFragment_to_loginFragment)

                }
            }


        } catch (e: Exception) {

            Log.d(SIGN_UP, e.localizedMessage)

            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")
            }


        }
    }


    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","INFO"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }


}
