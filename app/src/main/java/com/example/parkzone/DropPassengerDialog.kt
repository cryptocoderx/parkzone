package com.example.parkzone

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.navigation.Navigation
import com.example.parkzone.databinding.BookdialogBinding
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class DropPassengerDialog: DialogFragment() {


    private lateinit var user_type: String;


    lateinit var binding: BookdialogBinding



    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bookdialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = BookdialogBinding.bind(view);


        binding.parkingName.text = "DROP PASSENGER"


        binding.submitBook.setOnClickListener {


            val   plateNo = binding.plateNo.text.toString().trim { it<= ' '  } // trim fist and last space
            val  description = binding.description.text.toString().trim { it<= ' '  }


            if(plateNo.isEmpty())
            {
                ToastMessage("Plate number is required", "WARNING")
                return@setOnClickListener

            }

            if(description.isEmpty())
            {
                ToastMessage("Vehicle description is required", "WARNING")
                return@setOnClickListener

            }


            CoroutineScope(Dispatchers.IO).launch {

                val document = FirebaseInstance.firestoreDB.collection("UserProfile").
                document(FirebaseInstance.firebaseAuth.currentUser!!.uid).get().await()

                val  data = document.toObject(UserProfile::class.java)

                if (data!!.UserType == 1)

                { user_type= "Employee" } else {user_type= "Student"}


                val reserveSlots = ReserveSlots(
                    FirebaseInstance.firebaseAuth.currentUser!!.uid,
                    plateNo,
                    description,
                    "0",
                    "DROP PASSENGER",
                    data.IdNumber,
                    data.Firstname.plus(" "+ data.Lastname),
                    user_type,
                    "0.0",
                   "0.0",
                   Timestamp.now(),
                    "DROP_PASSENGER"
                )

                bookingProcess(view, reserveSlots);

            }
        }

        binding.cancelbook.setOnClickListener {

            dismiss()
        }

    }

    private suspend fun bookingProcess(view: View, reserveSlots: ReserveSlots){

        try {

            FirebaseInstance.firestoreDB.collection(FirebaseInstance.ReserveSlots).add(reserveSlots).await()

            //    val documentsRef  =   firestoreDB.collection(FirebaseInstance.ParkingLots)
            //       .whereEqualTo("Id", reserveSlots.SlotId)

            withContext(Dispatchers.Main){

                ToastMessage("Transaction Success", "SUCCESS")
                Navigation.findNavController(view).navigate(R.id.action_signUpFragment_to_loginFragment)

            }


        } catch (e: Exception) {

            Log.d("BOOKING", e.localizedMessage)

            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")

                dismiss()
            }


        }
    }

    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","WARNING"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }



}