package com.example.parkzone

import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import com.example.parkzone.databinding.FragmentMainBinding
import com.example.parkzone.databinding.FragmentProfileBinding
import com.shashank.sony.fancytoastlib.FancyToast


class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentProfileBinding.bind(view)


        val docRefProfile =  FirebaseInstance.firestoreDB.collection(FirebaseInstance.UserProfile)
            .document(FirebaseInstance.firebaseAuth.currentUser!!.uid)
        docRefProfile.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(BookDialog.TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null) {

                val Firstname: String = snapshot.data!!["Firstname"].toString()
                val Lastname: String = snapshot.data!!["Lastname"].toString()
                val IdNumber: String = snapshot.data!!["IdNumber"].toString()
                val Email: String = snapshot.data!!["Email"].toString()
                val PhoneNumber: String = snapshot.data!!["PhoneNumber"].toString()
                val Rating: String = snapshot.data!!["Rating"].toString()

                binding.firstname.setText(Firstname)
                binding.lastname.setText(Lastname)
                binding.IdNumber.setText(IdNumber)
                binding.email.setText(Email)
                binding.phone.setText(PhoneNumber)
                binding.rating.setText(Rating)



            }else{

                ToastMessage("Unable to locate user account","INFO")
            }

        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                ToastMessage("Please use the back button","INFO")
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        binding.backBtn.setOnClickListener{

            Navigation.findNavController(view).navigate(R.id.action_profileFragment_to_mainFragment)
        }


    }




    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","INFO"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }

}