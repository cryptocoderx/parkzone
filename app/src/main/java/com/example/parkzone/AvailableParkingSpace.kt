package com.example.parkzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.parkzone.Adapters.ParkingLotsAdapter
import com.example.parkzone.databinding.FragmentAvailableParkingSpaceBinding
import com.google.firebase.firestore.EventListener


class AvailableParkingSpace : Fragment() {

    private  lateinit var binding: FragmentAvailableParkingSpaceBinding
    private lateinit var adapter: ParkingLotsAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
    val view: View = inflater.inflate(R.layout.fragment_available_parking_space, container, false)


        binding = FragmentAvailableParkingSpaceBinding.bind(view);
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.setHasFixedSize(true)

        return view;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        adapter = ParkingLotsAdapter(
            ParkingLotsAdapter.OnClickListener { data ->
                activity?.let {
                 //   BookDialog.newInstance(data.Id.toString(),data.SlotName.toString(),data.Latitude.toString(),data.Longitude.toString()).show(it.supportFragmentManager, "SimpleDialog")
                }
            }
        )

        val documentsRef  =  FirebaseInstance.firestoreDB.collection(FirebaseInstance.ParkingLots)
            .whereEqualTo("Status", "VACANT")

        documentsRef.addSnapshotListener(EventListener { documentSnapshots, e ->
            if (e != null) {
                Log.e("DOCS", "Listen failed!", e)
                return@EventListener
            }

           val data =  documentSnapshots?.toObjects(ParkingLots::class.java);

           Log.d("DD",documentSnapshots?.toMutableList().toString())

            adapter.submitList(data)
            binding.recyclerView.adapter = adapter;
        })




    }
}