package com.example.parkzone

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.parkzone.FirebaseInstance.Companion.firebaseAuth
import com.example.parkzone.FirebaseInstance.Companion.firestoreDB
import com.example.parkzone.databinding.BookdialogBinding
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FieldValue
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class BookDialog: DialogFragment() {


    private lateinit var user_type: String;


lateinit var binding: BookdialogBinding

    companion object {

        const val TAG = "SimpleDialog"

        private const val KEY_ID = "KEY_ID"
        private const val KEY_NAME = "KEY_NAME"

        private const val KEY_LAT= "LAT"
        private const val KEY_LONG = "LONG"


        fun newInstance(id: String, name: String, lat: String, long: String): BookDialog {
            val args = Bundle()
            args.putString(KEY_ID, id)
            args.putString(KEY_NAME, name)
            args.putString(KEY_LAT, lat)
            args.putString(KEY_LONG, long)
            val fragment = BookDialog()
            fragment.arguments = args
            return fragment
        }

    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bookdialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
           binding = BookdialogBinding.bind(view);


        binding.parkingName.text = arguments?.getString(KEY_NAME)


        binding.submitBook.setOnClickListener {


         val   plateNo = binding.plateNo.text.toString().trim { it<= ' '  } // trim fist and last space
          val  description = binding.description.text.toString().trim { it<= ' '  }


            if(plateNo.isEmpty())
            {
                ToastMessage("Plate number is required", "WARNING")
                return@setOnClickListener

            }

            if(description.isEmpty())
            {
                ToastMessage("Vehicle description is required", "WARNING")
                return@setOnClickListener

            }

            ToastMessage("Loading...", "INFO")
            CoroutineScope(Dispatchers.IO).launch {

             val document = firestoreDB.collection("UserProfile").
             document(firebaseAuth.currentUser!!.uid).get().await()

               val  data = document.toObject(UserProfile::class.java)

           if (data!!.UserType == 1)

           { user_type= "Employee"
           } else {
               user_type= "Student"}


                val reserveSlots = ReserveSlots(
                    firebaseAuth.currentUser!!.uid,
                    plateNo,
                    description,
                    arguments?.getString(KEY_ID),
                    arguments?.getString(KEY_NAME),
                    data.IdNumber,
                    data.Firstname.plus(" "+ data.Lastname),
                    user_type,
                    arguments?.getString(KEY_LAT),
                    arguments?.getString(KEY_LONG),
                    Timestamp.now(),
                    "PENDING",
                )






                bookingProcess(view, reserveSlots);

            }
        }

        binding.cancelbook.setOnClickListener {

            dismiss()
           // Navigation.findNavController(view).navigate(R.id.action_bookDialog_to_parkingLotList)

        }

    }

    private suspend fun bookingProcess(view: View, reserveSlots: ReserveSlots){

        try {

            firestoreDB.collection(FirebaseInstance.ReserveSlots).add(reserveSlots).await()

            firestoreDB.collection(FirebaseInstance.ParkingLots).document(reserveSlots.SlotId.toString())
                .update("Status","RESERVED").await()

            firestoreDB.collection(FirebaseInstance.UserProfile).document(firebaseAuth.currentUser!!.uid)
                .update("Transaction","PENDING_BOOKING").await()


            withContext(Dispatchers.Main){

                ToastMessage("Transaction Success", "SUCCESS")
                dismiss()
             //   Navigation.findNavController(view)
            //    view.findNavController().navigate(R.id.action_bookDialog_to_mainFragment)

             //   val navController = Navigation.findNavController(requireActivity(),R.id.fragmentContainerView)
              //  navController.navigate(R.id.action_bookDialog_to_mainFragment)
            }


        } catch (e: Exception) {

            Log.d("BOOKING", e.localizedMessage)

            withContext(Dispatchers.Main){

              ToastMessage(e.localizedMessage, "ERROR")


            }


        }
    }

    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","WARNING"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }


}