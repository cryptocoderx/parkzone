package com.example.parkzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.google.firebase.analytics.FirebaseAnalytics
import com.shashank.sony.fancytoastlib.FancyToast
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class QRCodeScanner : Fragment() {

    private lateinit var data: ReserveSlots;
    private lateinit var data1: DroPassengersModel;
    private lateinit var codeScanner: CodeScanner
    val args: QRCodeScannerArgs by navArgs()
    private lateinit var Calltype: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_q_r_code_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val scannerView = view.findViewById<CodeScannerView>(R.id.scanner_view)
        val activity = requireActivity()
        Calltype =  args.calltype.toString()


        codeScanner = CodeScanner(activity, scannerView)
        codeScanner.decodeCallback = DecodeCallback {

            CoroutineScope(Dispatchers.IO).launch {

                 if(Calltype.equals("PARKING")){

                     GetParkingTransactionDetails(view,it.toString())

                 }else if(Calltype.equals("DROPPING")){

                    GetDroppingTransactionDetails(view,it.toString())


                 }else{

                     withContext(Dispatchers.Main){

                         ToastMessage("Undefined transaction type!","INFO")
                     }
                 }

            }


        }
        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }


        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                if(Calltype.equals("PARKING")){

                    Navigation.findNavController(view).navigate(R.id.action_QRCodeScanner_to_reservedParkingSpaces)

                }else if(Calltype.equals("DROPPING")){


                    Navigation.findNavController(view).navigate(R.id.action_QRCodeScanner_to_dropPassengerList)
                }

            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()

    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }


    private suspend fun GetParkingTransactionDetails(view: View, userCode: String){

        try {

            val checkData = FirebaseInstance.firestoreDB.collection(FirebaseInstance.ReserveSlots)
                .whereEqualTo("userCode", userCode)
                .whereIn("status",listOf("PENDING", "OCCUPIED"))
                .limit(1)
                .get().await()

            if (checkData.size() == 0 || checkData.isEmpty)
            {

                withContext(Dispatchers.Main){

                    ToastMessage("No reserved slots found!", "INFO")
                    Navigation.findNavController(view).navigate(R.id.action_QRCodeScanner_to_reservedParkingSpaces)

                }

            }else {

                for (document in checkData.getDocuments()) {
                    data = document.toObject(ReserveSlots::class.java)!!

                }

                withContext(Dispatchers.Main){

                    val action = QRCodeScannerDirections.actionQRCodeScannerToVerifyVehicle(
                        data.Name.toString(),data.SlotName.toString(),data.PlateNumber.toString(),data.VehicleDescription.toString(),
                        data.Status.toString(),data.SlotId.toString(),"PARKING",userCode)
                       Navigation.findNavController(view).navigate(action)
                }

            }


        } catch (e: Exception) {

            Log.d(FirebaseAnalytics.Event.SIGN_UP, e.localizedMessage)

            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")
            }


        }
    }

    private suspend fun GetDroppingTransactionDetails(view: View, userCode: String){

        try {

            val checkData = FirebaseInstance.firestoreDB.collection(FirebaseInstance.DroppingPassenger)
                .whereEqualTo("userCode", userCode)
                .whereIn("status",listOf("PENDING","UNLOADING","TIMES-UP"))
                .limit(1)
                .get().await()

            if (checkData.size() == 0 || checkData.isEmpty)
            {

                withContext(Dispatchers.Main){

                    ToastMessage("No unloading request found!", "INFO")

                    Navigation.findNavController(view).navigate(R.id.action_QRCodeScanner_to_dropPassengerList)

                }

            }else {

                for (document in checkData.getDocuments()) {
                    data1 = document.toObject(DroPassengersModel::class.java)!!

                }

                withContext(Dispatchers.Main){

                    val action = QRCodeScannerDirections.actionQRCodeScannerToVerifyVehicle(
                        data1.Name.toString(),"UNLOADING",data1.PlateNumber.toString(),data1.PlateNumber.toString(),
                        data1.Status.toString(),"CAMPUS","DROPPING",userCode)
                    Navigation.findNavController(view).navigate(action)
                }

            }


        } catch (e: Exception) {

            Log.d(FirebaseAnalytics.Event.SIGN_UP, e.localizedMessage)

            withContext(Dispatchers.Main){
                ToastMessage(e.localizedMessage, "ERROR")
            }


        }
    }


    private fun ToastMessage(message: String, type: String){

        when (type){
            in  "LOADING","ERROR","INFO"-> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.INFO,true).show();
            else -> FancyToast.makeText(activity,message, FancyToast.LENGTH_LONG, FancyToast.SUCCESS,true).show();
        }

    }

}