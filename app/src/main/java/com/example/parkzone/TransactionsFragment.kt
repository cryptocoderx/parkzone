package com.example.parkzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.parkzone.Adapters.ReserveSlotsAdapter
import com.example.parkzone.Adapters.TransactionAdapters
import com.example.parkzone.databinding.FragmentReservedParkingSpacesBinding
import com.example.parkzone.databinding.FragmentTransactionsBinding
import com.google.firebase.firestore.EventListener
import kotlinx.coroutines.tasks.await


class TransactionsFragment : Fragment() {
    lateinit var binding: FragmentTransactionsBinding
    private lateinit var adapter: TransactionAdapters


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
    val    view: View = inflater.inflate(R.layout.fragment_transactions, container, false)

        binding = FragmentTransactionsBinding.bind(view)
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.setHasFixedSize(true)


        return view
        return  view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)


        adapter = TransactionAdapters(
            TransactionAdapters.OnClickListener { data ->
                activity?.let {

                    //   BookDialog.newInstance(data.Id.toString(),data.SlotName.toString(),data.Latitude.toString(),data.Longitude.toString()).show(it.supportFragmentManager, "SimpleDialog")
                }
            }
        )


        val documentsRef  =  FirebaseInstance.firestoreDB.collection(FirebaseInstance.ReserveSlots)
            .whereEqualTo("userCode", FirebaseInstance.firebaseAuth.currentUser!!.uid)

        documentsRef.addSnapshotListener(EventListener { documentSnapshots, e ->
            if (e != null) {
                Log.e("DOCS", "Listen failed!", e)
                return@EventListener
            }

            val data =  documentSnapshots!!.toObjects(ReserveSlots::class.java);

            Log.e("DOCS", FirebaseInstance.firebaseAuth.currentUser!!.uid, e)
            adapter.submitList(data)
            binding.recyclerView.adapter = adapter;
        })


        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                Navigation.findNavController(view).navigate(R.id.action_transactionsFragment_to_mainFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

    }


}